package hirondelle.predict.pub.search;

import hirondelle.web4j.model.Check;
import hirondelle.web4j.model.Validator;
import hirondelle.web4j.security.SafeText;

public aspect SearchDAOAspect {

	pointcut interceptarBuscaTermos(SearchCriteria aSearchCriteria) :
		execution(* hirondelle.predict.pub.search.SearchDAO.getSearchTerms(..)) &&
		args(aSearchCriteria);
	
	after(SearchCriteria aSearchCriteria)
	returning(String[] resultado) : 
	interceptarBuscaTermos(aSearchCriteria) {
		if(resultado.length == 1) {
			System.out.println("\n\n>>> ASPECTO ATIVADO::. APENAS 1 TERMO CONSULTADO\n\n");
		}
		if(resultado.length > 1) {
			System.out.println("\n\n>>> ASPECTO ATIVADO::. MAIS DE 1 TERMO CONSULTADO\n\n");
		}		
	}

}
