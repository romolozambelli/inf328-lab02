package hirondelle.predict.aspect;

import static hirondelle.predict.pub.register.RegisterAction.ADD_NEW_USER;
import static hirondelle.predict.pub.register.RegisterAction.ADD_NEW_USER_ROLE;
import hirondelle.predict.pub.register.PasswordHasher;
import hirondelle.predict.pub.register.Register;
import hirondelle.web4j.database.DAOException;
import hirondelle.web4j.database.SqlId;
import hirondelle.web4j.database.TxSimple;
import hirondelle.web4j.model.Id;

public privileged aspect InjecaoNull {



	pointcut interceptarUserInvalido(Register register) :
		execution(* hirondelle.predict.pub.register.RegisterDAO.add(..)) &&
		args(register);

	before(Register register) throws DAOException:
		interceptarUserInvalido(register) {
		String hashedPassword = PasswordHasher.hash(register.getPassword()
				.getRawString());
		SqlId[] sqls = new SqlId[] { ADD_NEW_USER, ADD_NEW_USER_ROLE };
		if (register.getEmail().getRawString().equals("falha@panico.com.br")) {
			Object[] params = new Object[] {
					/* params for user: */
					null, Id.from(hashedPassword), register.getEmail(),
					/* params for role: */
					null };
			TxSimple tx = new TxSimple(sqls, params);
			System.out.println(tx.executeTx());
		}
	}

	/**
	 *  Remocao da Validacao do Captcha:
	 *  
	 * Foram inseridos codigos para que o Captcha sempre retorne true, nao importa o valor inserido
	 */
	pointcut validateCaptcha(): execution (boolean hirondelle.predict.pub.register.Register.isCaptchaValid*(..));
	boolean around(): call(boolean isCaptchaValid()) {
		return true;
	}

	pointcut isCaptchaPresent(): execution (boolean hirondelle.predict.pub.register.Register.isCaptchaPresent*(..));
	boolean around(): call(boolean isCaptchaPresent()) {
		return true;
	}
}
