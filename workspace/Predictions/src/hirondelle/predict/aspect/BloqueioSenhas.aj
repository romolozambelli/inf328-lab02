package hirondelle.predict.aspect;

import hirondelle.web4j.model.ModelCtorException;
import hirondelle.web4j.security.SafeText;

public aspect BloqueioSenhas {

	/** Senhas que deverao ser bloqueadas */
	private final SafeText passwordsBlocked[] = { new SafeText("12344321a*"), new SafeText("12344321A*"), new SafeText("12345678.a") };

	pointcut interceptarValidacaoDeSenha(SafeText fPassword) :
		execution(* hirondelle.predict.pub.register.Register.validateState(..)) &&
		args(fPassword);
	after(SafeText fPassword) throws ModelCtorException:
		interceptarValidacaoDeSenha(fPassword) {
		ModelCtorException ex = new ModelCtorException();
		for (SafeText currentPasswordBlocked : passwordsBlocked) {
			if(fPassword.equals(currentPasswordBlocked)) {
				ex.add("This password cannot be used. Please contact our system administrator.");
			}
		}

		/**
		 *  Validando a senha:
		 *  
		 *  As senhas v�lidas devem possuir pelo menos 8 caracteres, sendo que dentre eles deve haver 
		 *  uma letra min�scula ou mai�scula, um caracter dentre �*�, �.�, �$� e �!�, e pelo menos dois
		 *  n�meros.
		 */
		if(!fPassword.getRawString().matches("((?=(.*\\d){2})(?=.*[a-zA-Z])(?=.*[*$.!]).{8,})")) {
			ex.add("The password informed does not follow the security policy.");
		}

		if ( ! ex.isEmpty() ) throw ex;
	}

}