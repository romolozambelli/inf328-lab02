package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(11);
    _jspx_dependants.add("/WEB-INF/JspHeader.jspf");
    _jspx_dependants.add("/WEB-INF/tlds/c.tld");
    _jspx_dependants.add("/WEB-INF/tlds/fmt.tld");
    _jspx_dependants.add("/WEB-INF/tlds/fn.tld");
    _jspx_dependants.add("/WEB-INF/tlds/web4j.tld");
    _jspx_dependants.add("/WEB-INF/TagHeader.jspf");
    _jspx_dependants.add("/WEB-INF/tags/headsimple.tag");
    _jspx_dependants.add("/WEB-INF/tags/showFocus.tag");
    _jspx_dependants.add("/WEB-INF/tags/headerAndMenu.tag");
    _jspx_dependants.add("/WEB-INF/tags/menu.tag");
    _jspx_dependants.add("/WEB-INF/tags/footer.tag");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write(" \r\n");
      out.write("  \r\n");
      out.write("\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> \r\n");
      out.write("<html>\r\n");
      out.write(" ");
      if (_jspx_meth_tags_005fheadsimple_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      if (_jspx_meth_tags_005fheaderAndMenu_005f0(_jspx_page_context))
        return;
      out.write(" \r\n");
      out.write("\r\n");
      out.write("<h2>So What's This All About ?</h2>\r\n");
      out.write("\r\n");
      out.write("This site is for storing, sharing, and searching <b>predictions</b>. Why? Just for fun...\r\n");
      out.write("\r\n");
      out.write("<P>The following features are available :\r\n");
      out.write("<ul>\r\n");
      out.write(" <li>you can create as many predictions as you want. \r\n");
      out.write(" <li>all predictions are publicly viewable and searchable.\r\n");
      out.write(" <li>you can assign your predictions a result or score.\r\n");
      out.write(" <li>you can arrange your predictions in separate lists.\r\n");
      out.write("</ul>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write(" ");
      if (_jspx_meth_tags_005ffooter_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_tags_005fheadsimple_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tags:headsimple
    org.apache.jsp.tag.web.headsimple_tag _jspx_th_tags_005fheadsimple_005f0 = new org.apache.jsp.tag.web.headsimple_tag();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_tags_005fheadsimple_005f0);
    _jspx_th_tags_005fheadsimple_005f0.setJspContext(_jspx_page_context);
    // /Home.jsp(4,1) name = title type = java.lang.String reqTime = true required = true fragment = false deferredValue = false expectedTypeName = java.lang.String deferredMethod = false methodSignature = null
    _jspx_th_tags_005fheadsimple_005f0.setTitle("Home");
    _jspx_th_tags_005fheadsimple_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_tags_005fheadsimple_005f0);
    return false;
  }

  private boolean _jspx_meth_tags_005fheaderAndMenu_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tags:headerAndMenu
    org.apache.jsp.tag.web.headerAndMenu_tag _jspx_th_tags_005fheaderAndMenu_005f0 = new org.apache.jsp.tag.web.headerAndMenu_tag();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_tags_005fheaderAndMenu_005f0);
    _jspx_th_tags_005fheaderAndMenu_005f0.setJspContext(_jspx_page_context);
    _jspx_th_tags_005fheaderAndMenu_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_tags_005fheaderAndMenu_005f0);
    return false;
  }

  private boolean _jspx_meth_tags_005ffooter_005f0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  tags:footer
    org.apache.jsp.tag.web.footer_tag _jspx_th_tags_005ffooter_005f0 = new org.apache.jsp.tag.web.footer_tag();
    org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_tags_005ffooter_005f0);
    _jspx_th_tags_005ffooter_005f0.setJspContext(_jspx_page_context);
    _jspx_th_tags_005ffooter_005f0.doTag();
    org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_tags_005ffooter_005f0);
    return false;
  }
}
